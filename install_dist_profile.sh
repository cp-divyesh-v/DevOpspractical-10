#!/usr/bin/env sh

DIST_PROFILE_FILE=${DIST_PROVISION_UUID}.mobileprovision
DIST_PROFILE_WIDGET_FILE=${DIST_PROVISION_WIDGET_UUID}.mobileprovision

# Recreate the certificate from the secure environment variable
echo $DIST_PROVISION | base64 --decode > $DIST_PROFILE_FILE
echo $DIST_PROVISION_WIDGET | base64 --decode > $DIST_PROFILE_WIDGET_FILE

# copy where Xcode can find it
cp ${DIST_PROFILE_FILE} "$HOME/Library/MobileDevice/Provisioning Profiles/${DIST_PROVISION_UUID}.mobileprovision"

cp ${DIST_PROFILE_WIDGET_FILE} "$HOME/Library/MobileDevice/Provisioning Profiles/${DIST_PROVISION_WIDGET_UUID}.mobileprovision"

# clean
rm -fr *.mobileprovision
