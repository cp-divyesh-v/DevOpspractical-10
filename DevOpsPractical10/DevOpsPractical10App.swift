//
//  DevOpsPractical10App.swift
//  DevOpsPractical10
//
//  Created by Divyesh on 28/09/24.
//

import SwiftUI
import FirebaseCore

@main
struct DevOpsPractical10App: App {
    @UIApplicationDelegateAdaptor(AppDelegate.self) var delegate
    var body: some Scene {
        WindowGroup {
            ContactListView()
        }
    }
}

class AppDelegate: NSObject, UIApplicationDelegate {
  func application(_ application: UIApplication,
                   didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
    FirebaseApp.configure()

    return true
  }
}
